<?php

namespace Mediapress\Tracker\Controllers;


use Carbon\Carbon;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use Mediapress\Http\Controllers\Controller;
use Mediapress\Tracker\Models\Config;
use Mediapress\Tracker\Models\Hsr;
use Mediapress\Tracker\Models\Track;
use Mediapress\Modules\Heraldist\Models\Message;

class TrackerController extends Controller
{
    public const CONTENT_TYPE = 'Content-Type';
    public $api = 'https://editor.mclck.com/';
    public const BREAKER = '/*EDITOR*/';
    public const CONTENTS = 'contents';
    public const QUOT = '&quot;';
    public const OPTIONS = 'options';
    public const PARAMS = 'params';

    public function index()
    {

    }

    public function show(Request $request)
    {
        if ($request->module) {
            return $this->module($request);
        }


        $message = Message::find($request->id);
        $tracker_id = $message->track_id;


        Session::put('flowTrackId', $tracker_id);
        $hsrs = Hsr::where('hsr_vid', $tracker_id)->get();
        $hsr = $hsrs->first();
        $events = [];
        $duration = 0;
        foreach ($hsrs as $hsr) {
            if (is_array($hsr->hsr_ev)) {
                foreach ($hsr->hsr_ev as $ev) {
                    $ti = isset($ev['ti']) ? $ev['ti'] : 0;

                    $events[] = [
                        'time_since_load' => $ti,
                        'event_type' => isset($ev['ty']) ? $ev['ty'] : 1,
                        'x' => isset($ev['x']) ? $ev['x'] : 0,
                        'y' => isset($ev['y']) ? $ev['y'] : 0,
                        'selector' => isset($ev['s']) ? $ev['s'] : null,
                        'text' => isset($ev['te']) ? $ev['te'] : null,
                    ];
                    if ($ti > $duration) {
                        $duration = $ti;
                    }
                }
            }

        }
        $recordingData = ['events' => $events];
        $recordingData['viewport_w_px'] = $hsr->hsr_vw;
        $recordingData['viewport_h_px'] = $hsr->hsr_vh;
        $recordingData['pageviews'] = json_decode('[{"label":"3renk.test\/tr\/projeler","idvisitor":"19442ddabaae42b5","idloghsr":"1","time_on_page":"62738","resolution":"901x895","server_time":"2019-04-08 15:49:22","scroll_y_max_relative":"925","fold_y_relative":"682","server_time_pretty":"8 Nis 2019 16:49:22","time_on_page_pretty":"1 dakika 2saniye"},{"label":"3renk.test\/tr\/iletisim","idvisitor":"19442ddabaae42b5","idloghsr":"2","time_on_page":"130288","resolution":"1167x895","server_time":"2019-04-08 15:49:52","scroll_y_max_relative":"595","fold_y_relative":"595","server_time_pretty":"8 Nis 2019 16:49:52","time_on_page_pretty":"2 dakika 10saniye"},{"label":"3renk.test\/tr\/iletisim","idvisitor":"19442ddabaae42b5","idloghsr":"3","time_on_page":"46747","resolution":"1167x895","server_time":"2019-04-08 15:52:03","scroll_y_max_relative":"595","fold_y_relative":"595","server_time_pretty":"8 Nis 2019 16:52:03","time_on_page_pretty":"46sn"},{"label":"3renk.test\/tr\/iletisim","idvisitor":"19442ddabaae42b5","idloghsr":"4","time_on_page":"132800","resolution":"1167x895","server_time":"2019-04-08 15:53:10","scroll_y_max_relative":"595","fold_y_relative":"595","server_time_pretty":"8 Nis 2019 16:53:10","time_on_page_pretty":"2 dakika 12saniye"},{"label":"3renk.test\/tr\/iletisim","idvisitor":"19442ddabaae42b5","idloghsr":"5","time_on_page":"1700","resolution":"1920x895","server_time":"2019-04-08 15:53:19","scroll_y_max_relative":"595","fold_y_relative":"595","server_time_pretty":"8 Nis 2019 16:53:19","time_on_page_pretty":"1sn"},{"label":"3renk.test\/tr\/iletisim","idvisitor":"19442ddabaae42b5","idloghsr":"6","time_on_page":"87172","resolution":"1167x895","server_time":"2019-04-08 15:53:21","scroll_y_max_relative":"595","fold_y_relative":"595","server_time_pretty":"8 Nis 2019 16:53:21","time_on_page_pretty":"1 dakika 27saniye"},{"label":"3renk.test\/tr\/iletisim","idvisitor":"19442ddabaae42b5","idloghsr":"7","time_on_page":"900515","resolution":"1167x895","server_time":"2019-04-08 15:54:50","scroll_y_max_relative":"701","fold_y_relative":"595","server_time_pretty":"8 Nis 2019 16:54:50","time_on_page_pretty":"15 dakika 0saniye"}]',
            1);
        $recordingData['idLogHsr'] = 1;
        $recordingData['idSiteHsr'] = 1;
        $recordingData['idSite'] = 1;
        $recordingData['duration'] = $duration;
        $recordingData['url'] = $hsr->url;
        //Carbon::setLocale('tr_TR.utf8');
        $recordingData['time'] = Carbon::parse($hsr->created_at)->formatLocalized('%d %B %Y - %H:%M');

        return view('TrackerPanel::show', compact('recordingData'));
    }

    public function sessionvisDirective(Request $request)
    {
        return view('TrackerPanel::Angular.sessionvis_directive');
    }

    private function module(Request $request)
    {
        $hsr = Hsr::where('hsr_vid', session('flowTrackId'))->first();
        return view('TrackerPanel::Angular.'.$request->module, compact('hsr'));
    }

    public function matomo(Request $request)
    {
        if ($request->hsr_vid) {
            return $this->record($request);
        }

        Track::create($request->all());
        $file = file_get_contents(__DIR__.'/../Assets/matomo.gif');
        return response($file)->header(self::CONTENT_TYPE, 'image/gif');
    }

    public function recording(Request $request)
    {
        Config::create($request->all());
        return response('Piwik.HeatmapSessionRecording.configuration.assign({"heatmaps":[],"sessions":[{"id":1,"sample_rate":"10.0","min_time":0,"activity":true,"keystrokes":true}],"idsite":"1","trackerid":"'.$request->trackerid.'"});')->header(self::CONTENT_TYPE,
            'application/javascript');
    }

    public function post(Request $request)
    {
        Hsr::create($request->all());
        return response()->json($request->all());
    }

    private function record(Request $request)
    {
        Hsr::create($request->all());
        $file = file_get_contents(__DIR__.'/../Files/matomo.gif');
        return response($file)->header(self::CONTENT_TYPE, 'image/gif');
    }

}
