<?php

namespace Mediapress\Tracker\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $connection = 'mediapress_interaction';
    protected $table = 'sessions';
    protected $fillable = ["cookie_id", 'hash'];

    public function cookie()
    {
        return $this->belongsTo(Cookie::class);
    }

    public function visits()
    {
        return $this->hasMany(Visit::class);
    }
}
