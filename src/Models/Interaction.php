<?php

namespace Mediapress\Tracker\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Mediapress\Foundation\UserAgent\UserAgent;
use Mediapress\Tracker\Models\Visit;
use Illuminate\Database\Eloquent\Model;

class Interaction extends Model
{
    public const REFERER = 'referer';
    public $name = "Interaction";
    public $url = "mp-admin/Interaction";
    public $description = "Interaction";
    public $author = "";
    public $menus = [];
    private $plugins = [];


    public function checkCookie()
    {
        $request = Request::capture();
        if ($request->segment(1) == 'mp-admin') {
            return null;
        }

        $cookieValue = Cookie::get('mediapress');

        if (!$cookieValue) {
            $cookie = $this->createCookie();

        } else {
            $cookie = \Mediapress\Tracker\Models\Cookie::where('hash', $cookieValue)->first();
            if (!$cookie) {

                $cookie = $this->createCookie();
            }
        }

        $sessionValue = Session::get('mediapressSession');

        if (!$sessionValue) {
            $sessionHash = $this->makeUniqueId();

            $session = \Mediapress\Tracker\Models\Session::updateOrCreate(
                ['hash' => $sessionHash],
                [
                    'hash' => $sessionHash,
                    'cookie_id' => $cookie->id
                ]
            );

            Session::put('mediapressSession', $sessionHash);

        } else {
            $session = \Mediapress\Tracker\Models\Session::where('hash', $sessionValue)->first();
        }

        return $this->makeRequest($cookie, $session);

    }

    private function createCookie()
    {
        $cookieHash = $this->makeUniqueId();

        $cookie = \Mediapress\Tracker\Models\Cookie::updateOrCreate(
            ['hash' => $cookieHash],
            [
                'hash' => $cookieHash
            ]
        );

        Cookie::queue(Cookie::forever('mediapress', $cookieHash));

        return $cookie;
    }

    private function makeUniqueId()
    {
        return md5(uniqid(time(), true));
    }

    private function makeRequest($cookie, $session)
    {
        $request = Request::capture();

        $visit = [
            'cookie_id' => $cookie->id,
            'session_id' => $session->id
        ];

        $query_string = '';
        $i = 0;
        $utm = [];
        foreach ($request->all() as $key => $value) {
            if (!is_array($value)) {
                if (strpos($key, 'utm_') !== false) {
                    $utm[$key] = $value;
                }
                if ($value) {
                    if ($i == 0) {
                        $query_string .= '?'.$key.'='.urlencode($value);
                    } else {
                        $query_string .= '&'.$key.'='.urlencode($value);
                    }
                }
                $i++;
            }
        }

        if (count($utm)) {
            Session::put('mediapressUtm', $utm);
        }

        $referer = $request->header(self::REFERER);
        if ($referer) {
            $parsedUrl = parse_url($referer);
            if (isset($parsedUrl['host'])) {
                $host = $request->server('HTTP_HOST');
                if ($host != $parsedUrl['host']) {
                    Session::put('mediapressSource', $referer);
                }
            }
        }

        $visit['query_string'] = $query_string;
        $visit['url'] = $request->path();
        $visit[self::REFERER] = $request->headers->get(self::REFERER);

        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        if (!isset($_SERVER["REMOTE_ADDR"])) {
            $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        }

        $ua = new UserAgent();

        $visit['ip'] = $_SERVER['REMOTE_ADDR'];
        $visit['user_agent'] = $ua->getUserAgent();
        $visit['browser'] = $ua->browser();
        $visit['device'] = $ua->getDevice();
        $visit['os'] = $ua->platform();

        return Visit::create($visit);
    }


}
