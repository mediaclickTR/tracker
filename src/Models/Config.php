<?php

namespace Mediapress\Tracker\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $connection = 'mediapress_flow';
    protected $table = 'configs';


    protected $fillable = ["trackerid", 'url', 'hsr1'];

}
