<?php

namespace Mediapress\Tracker\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class Cookie extends Model
{
    use CacheQueryBuilder;
    protected $connection = 'mediapress_interaction';
    protected $table = 'cookies';
    protected $fillable = ["hash", 'user_id'];

    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    public function visits()
    {
        return $this->hasMany(Visit::class);
    }
}
