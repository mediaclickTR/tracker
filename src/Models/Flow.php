<?php

namespace Mediapress\Tracker\Models;

use Illuminate\Database\Eloquent\Model;

class Flow extends Model
{
    public $name = "Flow";
    public $url = "mp-admin/Flow";
    public $description = "Flow";
    public $author = "";
    public $menus = [];
    public $plugins = [];
}
