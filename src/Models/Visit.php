<?php

namespace Mediapress\Tracker\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class Visit extends Model
{
    use CacheQueryBuilder;
    protected $connection = 'mediapress_interaction';
    protected $table = 'visits';
    protected $fillable = ["cookie_id", 'session_id', 'url_id', 'url', 'query_string', 'referer', 'ip', "user_agent", "browser", "device", "os"];

    public function cookie()
    {
        return $this->belongsTo(Cookie::class);
    }

    public function session()
    {
        return $this->belongsTo(Session::class);
    }
}
