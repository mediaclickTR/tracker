<?php

namespace Mediapress\Tracker\Models;

use Illuminate\Database\Eloquent\Model;

class Hsr extends Model
{
    protected $connection = 'mediapress_flow';
    protected $table = 'hsrs';
    protected $fillable = [
        "hsr_vid",
        'hsr_ev',
        'hsr_ids',
        'hsr_vw',
        'hsr_vh',
        'hsr_ti',
        'hsr_smp',
        'hsr_fyp',
        'idsite',
        'rec',
        'r',
        'h',
        'm',
        's',
        'url',
        'urlref',
        '_id',
        '_idts',
        '_idvc',
        '_idn',
        '_refts',
        '_viewts',
        'send_image',
        'pdf',
        'qt',
        'realp',
        'wma',
        'dir',
        'fla',
        'java',
        'gears',
        'ag',
        'cookie',
        'res',
        'gt_ms',
        'pv_id',
    ];

    protected $casts = [
        'hsr_ids' => 'array',
        'hsr_ev' => 'array'
    ];

}
