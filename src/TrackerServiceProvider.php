<?php

namespace Mediapress\Tracker;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Mediapress\Tracker\Models\Flow;
use Mediapress\Tracker\Models\Interaction;
use Illuminate\Foundation\AliasLoader;

class TrackerServiceProvider extends ServiceProvider
{
    /**
     * Definitions
     */

    public const INTERACTION = "Interaction";
    public const FLOW = "Flow";
    public const DATABASE = 'database';
    public const RESOURCES = 'Resources';
    public const LANG = 'Lang';
    public const PANEL = 'Panel';
    public const MODULE_NAME = 'Tracker';
    public const CONFIG = 'Config';
    public const ACTIONS_PHP = 'Actions.php';
    public const DIRECTORY_SEPARATOR = '/';
    protected $namespace = 'Mediapress\Tracker';

    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.self::DIRECTORY_SEPARATOR.self::RESOURCES.self::DIRECTORY_SEPARATOR.self::LANG.self::DIRECTORY_SEPARATOR.self::PANEL.self::DIRECTORY_SEPARATOR.'Tr',
            self::MODULE_NAME.self::PANEL);

        $this->publishes([__DIR__.DIRECTORY_SEPARATOR.'Database' => storage_path('app'.DIRECTORY_SEPARATOR.self::DATABASE)],
            "Interaction".'Database');

        $this->publishes([__DIR__.DIRECTORY_SEPARATOR.'Database' => storage_path('app'.DIRECTORY_SEPARATOR.self::DATABASE)],
            "Flow".'Database');

        $this->publishes([
            __DIR__.'/Assets' => public_path('vendor/mediapress/tracker'),
        ], 'public');

        $this->loadViewsFrom(__DIR__.DIRECTORY_SEPARATOR.'Resources'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'Panel',
            self::MODULE_NAME.'Panel');

        $this->publishActions(__DIR__);

        $this->map();

        $files = $this->app['files']->files(__DIR__.'/Config');

        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
    }

    public function register()
    {
        $this->app['config']->set("database.connections.mediapress_flow", [
            'driver' => 'sqlite',
            self::DATABASE => storage_path('app'.DIRECTORY_SEPARATOR.self::DATABASE.DIRECTORY_SEPARATOR.'Flow.sqlite'),
            'prefix' => ''
        ]);
        $loader = AliasLoader::getInstance();
        $loader->alias('Flow', Flow::class);

        app()->bind('Flow', function () {
            return new \Mediapress\Tracker\Models\Flow;
        });

        $this->app['config']->set("database.connections.mediapress_interaction", [
            'driver' => 'sqlite',
            self::DATABASE => storage_path('app'.DIRECTORY_SEPARATOR.self::DATABASE.DIRECTORY_SEPARATOR.'Interaction.sqlite'),
            'prefix' => ''
        ]);
        $loader = AliasLoader::getInstance();
        $loader->alias(self::INTERACTION, Interaction::class);

        app()->bind(self::INTERACTION, function () {
            return new \Mediapress\Tracker\Models\Interaction;
        });
    }

    protected function publishActions($dir)
    {
        $actions_config = $dir.DIRECTORY_SEPARATOR.self::CONFIG.DIRECTORY_SEPARATOR.self::MODULE_NAME.self::ACTIONS_PHP;

        if (is_file($actions_config)) {
            $this->publishes([
                $dir.DIRECTORY_SEPARATOR.self::CONFIG.DIRECTORY_SEPARATOR.self::MODULE_NAME.self::ACTIONS_PHP => config_path(strtolower(self::MODULE_NAME).'_module_actions.php')
            ]);

            $this->mergeConfigFrom($dir.DIRECTORY_SEPARATOR.self::CONFIG.DIRECTORY_SEPARATOR.self::MODULE_NAME.self::ACTIONS_PHP,
                strtolower(self::MODULE_NAME).'_module_actions');
        }
    }


    protected function getConfigBasename($file)
    {
        return preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($file));
    }

    protected function mergeConfig($path, $key)
    {

        $config = config($key);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        config([$key => $config]);
    }

    public function map()
    {
        $this->mapPanelRoutes();

        $this->mapWebRoutes();
    }

    protected function mapWebRoutes()
    {
        $routes_file = __DIR__.DIRECTORY_SEPARATOR.'Routes'.DIRECTORY_SEPARATOR.'WebRoutes.php';

        Route::group([
            'middleware' => 'flow',
            'namespace' => $this->namespace.'\Controllers',
        ], function ($router) use ($routes_file) {
            if (is_file($routes_file)) {
                include_once $routes_file;
            }
        });
    }

    protected function mapPanelRoutes()
    {
        $routes_file = __DIR__.DIRECTORY_SEPARATOR.'Routes'.DIRECTORY_SEPARATOR.'PanelRoutes.php';
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace.'\Controllers',
            'prefix' => 'mp-admin',
        ], function ($router) use ($routes_file) {
            if (is_file($routes_file)) {
                include_once $routes_file;
            }
        });
    }

}