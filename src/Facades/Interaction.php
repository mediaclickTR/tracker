<?php

namespace Mediapress\Tracker\Facades;

use Illuminate\Support\Facades\Facade;

class Interaction extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Tracker\Models\Interaction::class;
    }
}