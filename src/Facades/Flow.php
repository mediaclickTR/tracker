<?php

namespace Mediapress\Tracker\Facades;

use Illuminate\Support\Facades\Facade;

class Flow extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Tracker\Models\Flow::class;
    }
}