<?php

Route::group(['middleware' => 'panel.auth'], function () {
    Route::group(['prefix' => 'Tracker', 'as' => 'tracker.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'TrackerController@index']);
        Route::get('/show', ['as' => 'show', 'uses' => 'TrackerController@show']);

        Route::get('/plugins/HeatmapSessionRecording/angularjs/sessionvis/sessionvis.directive.html',
            ['as' => 'sessionvisDirective', 'uses' => 'TrackerController@sessionvisDirective']);
    });
});